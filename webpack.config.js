const path = require('path');

const { merge } = require("webpack-merge");
const singleSpaDefaults = require("webpack-config-single-spa-ts");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

module.exports = (webpackConfigEnv, argv) => {
  const orgName = "sreasons";
  const defaultConfig = singleSpaDefaults({
    orgName,
    projectName: "mf-root-config",
    webpackConfigEnv,
    argv,
    disableHtmlGeneration: true,
  });

  return merge(defaultConfig, {
    // modify the webpack config however you'd like to by adding to this object
    plugins: [
      new HtmlWebpackPlugin({
        inject: false,
        template: "src/index.ejs",
        templateParameters: {
          isLocal: webpackConfigEnv && webpackConfigEnv.isLocal,
          orgName,
        },
        title: 'SmartTube',
      }),
      new CopyPlugin({
        patterns: [
          {
            from: "./src/importmap.json",
            to({ context, absoluteFilename }) {
              return "importmap.json";
            },
          }
        ],
      }),
    ],
  });
};
