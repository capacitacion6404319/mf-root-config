import { registerApplication, start, LifeCycles, checkActivityFunctions } from "single-spa";
import * as singleSpa from 'single-spa'; 

/*registerApplication(
  {
    name: "@sreasons/mf-video",
    app: () => System.import<LifeCycles>("@sreasons/mf-video"),
    activeWhen: ["/"]
  }
);*/

registerApplication(
  {
    name: "@sreasons/mf-video",
    app: () => {
      try {
        return System.import<LifeCycles<{}>>("@sreasons/mf-video");
      } catch (error) {
        console.error('Error cargando la aplicación video:', error); 
      }
    },
    activeWhen: ["/"]
  }
);

/*registerApplication(
  {
    name: "@sreasons/mf-ng-video",
    app: () => {
      try {
        return System.import<LifeCycles<{}>>("@sreasons/mf-ng-video");
      } catch (error) {
        console.error('Error cargando la aplicación video:', error); 
      }
    },
    activeWhen: ["/"]
  }
);*/

registerApplication(
  {
    name: "@sreasons/mf-comentarios",
    app: () => System.import<LifeCycles>("@sreasons/mf-comentarios"),
    activeWhen: ["/"]
  }
);

/*registerApplication(
  {
    name: "@sreasons/mf-comentarios",
    app: async () => {
      try {
        const appModule = await System.import<LifeCycles<{}>>("@sreasons/mf-comentarios");
        return appModule;
      } catch (error) {
        console.error('Error cargando la aplicación comentarios:', error);        
      }
    },
    activeWhen: ["/"]
  }
);*/

/*registerApplication(
  {
    name: "@sreasons/mf-sugerencias",
    app: async () => {
      try {
        const appModule = await System.import<LifeCycles<{}>>("@sreasons/mf-sugerencias");
        return appModule;
      } catch (error) {
        console.error('Error cargando la aplicación sugerencias:', error);        
      }
    },
    activeWhen: ["/"]
  }
);*/

registerApplication(
  {
    name: "@sreasons/mf-sugerencias",
    app: () => System.import<LifeCycles>("@sreasons/mf-sugerencias"),
    activeWhen: ["/"]
  }
);

start({
  urlRerouteOnly: true,
});

window.addEventListener('single-spa:routing-event', (event) => {
  if (isInvalidRoute()) {
    singleSpa.navigateToUrl("/404");
  }
});

const isInvalidRoute = () => {
  const apps = checkActivityFunctions(window.location);
  return !apps.length;
}